alias virsh='virsh --connect qemu:///system'
alias ls='ls --color=auto -lh'
alias grep='grep --color=auto'
alias ip='ip --color=auto'

tmpclip () {
	read foo; echo -ne "$foo" | xclip -sel clip
	sleep 9
	xclip -sel clip < /dev/null
}
