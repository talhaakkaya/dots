syntax on
set number
set nowrap
set mouse=r
set shortmess=a
set laststatus=2
set showtabline=2
let g:netrw_liststyle = 3
let g:netrw_list_hide='.*\.swp$'
highlight LineNr ctermfg=grey

set cursorline
hi cursorline cterm=none term=none
autocmd WinEnter * setlocal cursorline
autocmd WinLeave * setlocal nocursorline
highlight CursorLine ctermbg=235
